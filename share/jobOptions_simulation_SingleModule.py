## Algorithm sequence

include("GeneratorUtils/StdEvgenSetup.py")

from AthenaCommon.GlobalFlags import globalflags
globalflags.ConditionsTag = "OFLCOND-MC15c-SDR-06"

from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags
SLHC_Flags.UseLocalGeometry.set_Value_and_Lock(True)

from AthenaCommon.AlgSequence import AlgSequence
topSeq = AlgSequence()

#--- Detector flags -------------------------------------------
from AthenaCommon.DetFlags import DetFlags
# - Select detectors 
#include("InDetSLHC_Example/preInclude.NoTRT_NoBCM_NoDBM.py")
DetFlags.ID_setOn()
#DetFlags.SCT_setOn()
#DetFlags.pixel_setOn()
#DetFlags.bpipe_setOn()
#DetFlags.Calo_setOn()
#DetFlags.LAr_setOn()
#DetFlags.em_setOn()
#DetFlags.Tile_setOn()
#DetFlags.Muon_setOff() # muons are not supported in >=13.0.0
# - MCTruth
DetFlags.Truth_setOn()

#--- AthenaCommon flags ---------------------------------------
from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
athenaCommonFlags.PoolHitsOutput='hits_output_myOwn.pool.root'
athenaCommonFlags.EvtMax=1000
athenaCommonFlags.PoolEvgenInput.set_Off()


#--- Simulation flags -----------------------------------------
from G4AtlasApps.SimFlags import simFlags
simFlags.load_atlas_flags()
#simFlags.SimLayout='ctbh8_combined'
#simFlags.LArEMBenergyCor=True 
#simFlags.Eta = 0.2
## Enable the EtaPhi, VertexSpread and VertexRange checks
#simFlags.EventFilter.set_On()

# Re-writes the constant magnetic field for the MBPSID with the map.  
#simFlags.MagFieldMap='mbps1-all-id-800-mbps2-muons-800.data'
#simFlags.MagFieldMapCalculation=1

#--- Generator flags ------------------------------------------

#simFlags.CalibrationRun.set_Value('LAr+Tile')

#--- Use single particle gun ----------------------------
import AthenaCommon.AtlasUnixGeneratorJob
import ParticleGun as PG
pg = PG.ParticleGun(randomSvcName=simFlags.RandomSvc.get_Value(), randomStream="SINGLE")
pg.sampler.pid = 11
pg.sampler.pos = PG.PosSampler(x=[-100,10], y=390, z=[-100,10])
pg.sampler.mom = PG.MXYZSampler(px=0, py=6000, pz=0)
topSeq += pg

include("G4AtlasApps/fragment.SimCopyWeights.py")

include("GeneratorUtils/postJO.CopyWeights.py")
include("GeneratorUtils/postJO.PoolOutput.py")
include("GeneratorUtils/postJO.DumpMC.py")

#---  Output printout level ----------------------------------- 
#output threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL)
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel = 3

## Use verbose G4 tracking
#from G4AtlasApps.SimFlags import simFlags
#simFlags.G4Commands += ['/tracking/verbose 1']

include("G4AtlasApps/G4Atlas.flat.configuration.py")

#==============================================================
# Job configuration
# ***>> Do not add flags or simulation options below this line
#==============================================================
## Populate alg sequence
from AthenaCommon.CfgGetter import getAlgorithm
topSeq += getAlgorithm("G4AtlasAlg",tryDefaultConfigurable=True)
topSeq.G4AtlasAlg.InputTruthCollection='GEN_EVENT'

from xAODTruthCnv.xAODTruthCnvConf import HepMCTruthReader
alg2 = HepMCTruthReader()
alg2.OutputLevel = INFO
alg2.HepMCContainerName = "GEN_EVENT"
topSeq += alg2
